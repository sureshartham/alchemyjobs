package AlchemyJobs;

import java.io.FileReader;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

public class Activity12_CreatingUserUsingExternalFile {
	WebDriverWait wait;
	WebDriver driver;
  @Test
  public void f() {
  }
  @BeforeClass
  public void beforeClass() throws IOException,CsvException{
	  
	  
	  
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/jobs/wp-admin");
	  
	  //Login to the page
	  driver.findElement(By.id("user_login")).sendKeys("root");
	  driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
	  driver.findElement(By.id("wp-submit")).click();
	  
	  driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/ul/li[11]/a/div[3]")).click();
	  driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/a")).click();
	  
	  CSVReader reader = new CSVReader(new FileReader("C:\\Users\\SureshArtham\\eclipse-workspace\\SeleniumTesting\\src\\resources\\User.csv"));
		
		//load content of the file into the list
		//List<String[]> list = reader.readAll();
		
		//System.out.println("The total no.of rows are "+list.size());
		
		//Create Iterator reference
      //Iterator<String[]> itr = list.iterator();
      //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  
	  String[] cell;
	     
          
      while((cell = reader.readNext()) != null)
      	
      {
      	          
           for(int i=0;i<1;i++) {
        	   
        	   String Username = cell[i];
               String eMail = cell[i+1];
               String firstName = cell[i+2];
               String lastName = cell[i+3];
               String website = cell[i+4];
               String passwrd = cell[i+5];
                               	  
        	   
        	   
        	   driver.findElement(By.id("user_login")).sendKeys(Username);
        		  
        		  driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(eMail);
        		       		     		
        		 
        		  driver.findElement(By.id("first_name")).sendKeys(firstName);
        		  driver.findElement(By.id("last_name")).sendKeys(lastName);
        		  
        		  driver.findElement(By.xpath("//*[@id=\"url\"]")).sendKeys(website); 		  
        		 		  	  
        		 /*
        		  
        		  driver.findElement(By.xpath("//button[contains(@class,'wp-generate-pw')]")).click();
            	  WebElement pwdButton = driver.findElement(By.id("pass1"));
        		  //WebElement pwdButton=driver.findElement(By.xpath("//button[contains(.,'Show password')]"));
        		   
            	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("pass1")));
            	  pwdButton.clear();
            	  
            	  ((JavascriptExecutor) driver).executeScript("arguments[0].value='" +passwrd + "';", pwdButton);
            	 driver.findElement(By.id("role")).sendKeys(role);
            	  
            	  //Add user click
                  Reporter.log("Clicking Add User button");
            	  driver.findElement(By.cssSelector("input#createusersub")).click(); */
            	  
            	  //Verify user is created
            	 
        		  
        		 // driver.findElement(By.id("pass1")).clear();
        		    WebElement element=driver.findElement(By.id("pass1"));
        		    element.isEnabled();
        		  
        		    ((JavascriptExecutor) driver).executeScript("arguments[0].value='" + passwrd + "';", element);
        		    driver.findElement(By.xpath("//input[@id='createusersub']")).click();
        		   
        		    //driver.findElement(By.xpath("//button[@class='button wp-generate-pw hide-if-no-js']")).click();
        		   //driver.findElement(By.className("button wp-generate-pw hide-if-no-js")).click();
        		    driver.findElement(By.cssSelector(".wp-generate-pw")).click();
        		  
        		  
            	  WebElement addSuccess = driver.findElement(By.cssSelector("#message > p:nth-child(1)"));
            	  System.out.println(addSuccess.getText());
            	  String message = "New user created. Edit user\nDismiss this notice.";
            	  Assert.assertEquals(message, addSuccess.getText());
        		  
        	   
           }
           reader.close();	
           driver.findElement(By.id("createusersub")).click();
           System.out.println(" ");
      }
     
      	
      	
      	
      }

	  
	  
	  


  @AfterClass
  public void afterClass() {
  }

}
