package AlchemyJobs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity2_WebsiteHeading {
	WebDriver driver;
	
  @Test
  public void f() {
  }
  @BeforeClass
  public void beforeClass() {
	  
	  WebElement WHeading;
	  
	  driver=new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/jobs");
	  
	  WHeading=driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/header/h1"));
			  
			  String heading=WHeading.getText();
	  
	  System.out.println("The heading of the page is " +heading);
	  Assert.assertEquals("Welcome to Alchemy Jobs", heading);
  }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
