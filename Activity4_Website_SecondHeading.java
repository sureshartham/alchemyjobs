package AlchemyJobs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity4_Website_SecondHeading {
	WebDriver driver;
	
  @Test
  public void f() {
  }
  @BeforeClass
  public void beforeClass() {
	  
	  driver=new FirefoxDriver();
	  
	  driver.get("https://alchemy.hguy.co/jobs");
	  
	  WebElement secondHeading=driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/h2"));
	  
	  String secondHeadingText=secondHeading.getText();
	  
	  System.out.println("The second heading is " +secondHeadingText);
	  
	  Assert.assertEquals("Quia quis non",secondHeadingText);
	  
	  
	  
  }

  @AfterClass
  public void afterClass() {
	  
	  driver.close();
  }

}
