package AlchemyJobs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.Assert;

public class Activity5_NavigateToAnotherPage {
	WebDriver driver;
	
  @Test
  public void f() {
  }
  @BeforeClass
  public void beforeClass() {
	  driver=new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/jobs");
	  
	  WebElement jobEle=driver.findElement(By.xpath("/html/body/div/header/div/div/div/div/div[3]/div/nav/div/ul/li[1]/a"));
	  
	  jobEle.click();
	  
	  WebElement nextPageTitle=driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/header/h1"));
	  String title =nextPageTitle.getText();
	  
	  System.out.println("The title of the page is " +title);
	  
	  Assert.assertEquals("Jobs" , title);
	  
  }

  @AfterClass
  public void afterClass() {
	  
	  driver.close();
  }
  
  
  

}
