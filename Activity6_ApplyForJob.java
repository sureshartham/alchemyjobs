package AlchemyJobs;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity6_ApplyForJob {
	WebDriver driver;
	
  @Test
  public void f() {
  }
  @BeforeClass
  public void beforeClass() {
	  
	  driver=new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/jobs");	  
      WebElement jobEle=driver.findElement(By.xpath("/html/body/div/header/div/div/div/div/div[3]/div/nav/div/ul/li[1]/a"));
	  jobEle.click();
	  
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  
	  driver.findElement(By.id("search_keywords")).sendKeys("Full Time");
	  driver.findElement(By.id("search_location")).sendKeys("Pune");
	  
	  
	  driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/form/div[1]/div[4]/input")).click();
	  
	  driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/ul/li/a/div[1]/h3")).click();
	  
	  driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/div/div[3]/input")).click();
	  
	  WebElement Email=driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/div/div[3]/div/p/a"));
	  
	  System.out.println("The applied job mail sent to " +Email.getText());
	  
	  
	  
	  
	  
	  
	  
	  
  }

  @AfterClass
  public void afterClass() {
	  
	  driver.close();
  }

}
