package AlchemyJobs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity8_loginBackend {
	WebDriver driver;
	
  @Test
  public void f() {
  }
  @BeforeClass
  public void beforeClass() {
	  
	  driver=new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/jobs/wp-admin");
	  driver.findElement(By.id("user_login")).sendKeys("root");
	  driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
	  driver.findElement(By.id("wp-submit")).click();
	  
	  System.out.println("The User login");
  }

  @AfterClass
  public void afterClass() {
	  
	  driver.close();
  }

}
